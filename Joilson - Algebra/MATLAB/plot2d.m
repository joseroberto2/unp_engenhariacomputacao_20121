% Fun��o para renderizar uma imagem 2d dado as coordenadas em uma
% Matriz coluna

% A ideia daqui � adaptar as coordenadas para a fun��o nativa "plot",
% transformando as coordenadas dos eixos em vetores coluna gerando assim
function plot2d (MatrizTransposta);
    
    % Atribui os dados da Matriz transposta � vari�vel "Matriz"
    Matriz = MatrizTransposta';
    
    % O ":" significa intervalo;
    
    % Monta as coordenadas de uma Matriz X dado o intervalo de todas as
    % linhas da primeira coluna da Matriz transposta;
    X = Matriz(:,1);
    
    % Monta as coordenadas de uma Matriz Y dado o intervalo de todas as
    % linhas da segunda coluna da Matriz transposta;
    Y = Matriz(:,2);
    
    % Renderizando a imagem usando a fun��o plot, passando as submatrizes
    % geradas pelas coordenadas de X e de Y, respectivamente
    plot (X, Y);
    
    % Ativa a grade para refer�ncia
    grid;
    
    % Determinando o limite da figura, para podermos ver o gr�fico tanto na
    % coordenada x, quanto na y; Recebe uma matriz linha com a coordenada
    % "de" e "para": no caso assumindo valores de -1 at� 2.
    xlim([-8 8]);
    ylim([-8 8]);
end