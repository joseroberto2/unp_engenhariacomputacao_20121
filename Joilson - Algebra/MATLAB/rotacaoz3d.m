% Rotaciona uma matriz em 3d no eixo Y dado a matriz transposta e seu �ngulo em Graus
function MatrizRetorno = rotacaoz3d( matrizTransposta, anguloGraus )

    % Convertendo o �ngulo em radianos e jogando o valor na vari�vel
    % "anguloRadiano"
    anguloRadiano = (anguloGraus*pi)/180;

    % Prepara a matriz de transforma��o fornecendo o anguloRadiano para
    % dentro das fun��es seno e cosseno
    transformacao = [ cos(anguloRadiano) -sin(anguloRadiano) 0; sin(anguloRadiano) cos(anguloRadiano) 0; 0 0 1 ];

    % A matriz de retorno � o resultado da multiplica��o das matrizes
    % transpostas "transformacao" e "matrizTransposta" que vai resultar na
    % matriz dos vetores coluna rotacionada
    MatrizRetorno = transformacao*matrizTransposta;

end