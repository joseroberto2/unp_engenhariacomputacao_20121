function Retorno = reflexo3dxy(MatrizTransposta);
    Transformacao = [ 1 0 0; 0 1 0; 0 0 -1 ]';
    
    Retorno = Transformacao*MatrizTransposta;
end
