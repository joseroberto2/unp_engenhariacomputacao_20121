% Esse script simples tende a facilitar a visualiza��o da aplica��o da
% matriz de rota��o em uma imagem em duas dimens�es.

% Consiste na execu��o de um la�o que vai varrer R de 0 at� 360 onde ser�
% invocado a fun��o plot, recebendo a transforma��o linear da rota��o,
% passando o quadrado como imagem e o �ngulo expresso por R;

for R = 0:360
    plot2d(rotacao2d(TRIANGULO, R));
    pause(0.001);
end