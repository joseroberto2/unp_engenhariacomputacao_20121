function Retorno = reflexo3dxz2d(MatrizTransposta);
    Transformacao = [ 1 0 ; 0 0; 0 1 ]';
    
    Retorno = Transformacao*MatrizTransposta;
end
