function Retorno = dilatarz3d(MatrizTransposta, fator);
    Transformacao = [ 1 0 0 ; 0 1 0; 0 0 fator]';
    Retorno = Transformacao*MatrizTransposta;
end
