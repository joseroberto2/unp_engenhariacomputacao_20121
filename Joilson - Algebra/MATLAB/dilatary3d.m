function Retorno = dilatary3d(MatrizTransposta, fator);
    Transformacao = [ 1 0 0 ; 0 fator 0; 0 0 1]';
    Retorno = Transformacao*MatrizTransposta;
end
