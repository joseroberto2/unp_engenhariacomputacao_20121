function Retorno = dilatarx2d(MatrizTransposta, fator);
    Transformacao = [ fator 0; 0 1]';
    Retorno = Transformacao*MatrizTransposta;
end